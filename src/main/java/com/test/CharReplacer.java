package com.test;

/**
 * Created by vvolovod on 5/18/16.
 */
public class CharReplacer {
    public static final char WHITESPACE = ' ';

    public String replace(String inputStr, char charToReplace) {
        if (inputStr == null) {
            throw new IllegalArgumentException("The inputStr can't be null");
        }
        return inputStr.replace(charToReplace, WHITESPACE);
    }
}
