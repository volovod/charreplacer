package com.test;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

/**
 * Created by vvolovod on 5/18/16.
 */
public class CharReplacerTest {

    private CharReplacer replacer = new CharReplacer();

    @Test
    public void shouldReplaceXWithWhitespace() throws Exception {
        //given
        String inputString = "thisXStringXdoesXnotXcontainX'ex'";
        char charToReplace = 'X';
        //when
        String result = replacer.replace(inputString, charToReplace);
        //then
        assertThat(result)
                .contains(String.valueOf(CharReplacer.WHITESPACE))
                .doesNotContain(String.valueOf(charToReplace))
                .matches("this String does not contain 'ex'");
    }

    @Test
    public void shouldReturnTheSameString() throws Exception {
        //given
        String inputString = "thisXStringXdoesXnotXcontainX'ex'";
        //when
        String result = replacer.replace(inputString, 'Y');
        //then
        assertThat(result).isSameAs(inputString);
    }

    @Test
    public void shouldReturnTheSameStringIfEmptyStringIsGiven() throws Exception {
        //given
        String inputString = "";
        //when
        String result = replacer.replace(inputString, 'Y');
        //then
        assertThat(result).isSameAs(inputString);
    }

    @Test
    public void shouldThrowExceptionIfInputStringIsNull() throws Exception {
        //given
        //when
        Throwable thrown = catchThrowable(() -> replacer.replace(null, 'Y'));
        //then
        assertThat(thrown)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("The inputStr can't be null")
                .hasNoCause();
    }
}